public enum Color
{
	//enum holding all the card's colors
	RED,
	YELLOW,
	GREEN,
	BLUE,
	BLACK //wildcards, lets player choose the color after their turn
}