import java.util.InputMismatchException;
import java.util.Scanner;
public class Player{
	
	private String name = "";
	private Deck deck;
	private boolean[] playableCards; //true if playable, index corresponds to the same one in deck
	private boolean playableCardExists;
	
	private Color forcedColor = Color.BLACK; //black = don't force anything


	private Scanner scan;
	
	public Player(String name)
	{
		this.name = name;
		this.scan = new Scanner(System.in);
		//deck will be empty and added to after Player's creation
		resetDeck();
	}

	private int selectCard()
	{
		//makes the player select a valid card
		System.out.println("Pick a <number>:");
		boolean cardChosen = false; // to keep player in loop until a valid card is picked
		int input = 0;
		while (!cardChosen) {
			try {
				input = scan.nextInt(); // todo handle when > length or < 0 (no errors pls)
				// validate
				if (input >= 0 && input < this.deck.length()) {
					// if selection between 0 and length
					if (playableCards[input]) // check if playablecards bool[] is valid @ input
					{
						// if card is playable escape the loop then process card						
						cardChosen = true;
					} else {
						System.out.println("Invalid Card, the value or the color must match!");
					}
				} else {
					System.out.println("Invalid Input!");
				}
			} catch (InputMismatchException e) {
				// reset scanner, print message
				System.out.println("Please enter a <number>!");
				scan = new Scanner(System.in);
			}

		}
		return input;
	}
	
	public Card playTurn(Deck discardDeck, Deck drawDeck, Color forcedColor) {
		// print cards in hand, and allow them to pick one based on the topmost discardDeck card.
		// Returns the selected card while removing it from the player
		this.forcedColor = forcedColor;
		setplayableCardInfo(discardDeck.getTop());
		// if no playable cards, draw until they do
		while (!playableCardExists) {
			this.draw(discardDeck, drawDeck);
		}
		// sort and update playable card bool[]
		deck.sort();
		setplayableCardInfo(discardDeck.getTop());
		deck.print(playableCards);
		int input = selectCard();
		// resets forcedColor at the end
		forcedColor = Color.BLACK;
		//remove and return chosen card
		Card chosenCard = deck.get(input);
		deck.remove(input);
		return chosenCard;
	}

	public void draw(Deck discardDeck, Deck drawDeck)
	{
		//draws from drawdeck and manages when drawdeck is empty
		System.out.println("You drew a card.");
		if(drawDeck.get(0) == null)
		{
			//if null, transfer from discard to draw
			//System.out.println("The draw pile is empty, so you took all the cards in the discard pile and put it in the draw pile.");
			discardDeck.takeAll(drawDeck, true);
			if(drawDeck.get(0) != null) //if there's still no cards in the draw pile just don't draw anything (ultra rare case)
			{
				drawDeck.transfer(0, this.deck);
				setplayableCardInfo(discardDeck.getTop()); //and refresh playableCard info
			}
		}
		else{
			drawDeck.transfer(0, this.deck);
			setplayableCardInfo(discardDeck.getTop()); //and refresh playableCard info
		}		
	}

	public String getName()
	{
		return this.name;
	}

	public String toString()
	{
		return this.name;
	}

	private void setplayableCardInfo(Card discardPileCard)
	{
		//sets the playableCard bool[] to have it so each index = the index of a card in the deck
		
		playableCards = new boolean[deck.length()];
		for (int i = 0; i < playableCards.length; i++)
		{
			//set black cards valid automatically
			if(deck.get(i).getColor() == Color.BLACK)
			{
				playableCards[i] = true;		
			}
			else
			{
				//else check for color and value
				if (this.forcedColor == Color.BLACK)
				{
					//don't force a color
					playableCards[i] = (deck.get(i).getColor() == discardPileCard.getColor() || deck.get(i).getValue() == discardPileCard.getValue());
				}
				else
				{
					//force a color to be used
					playableCards[i] = (deck.get(i).getColor() == forcedColor || deck.get(i).getValue() == discardPileCard.getValue());
				}
			}
			
								
		}

		//also sets the bool playableCardExists if there's > one true value in the array
		for(int i = 0; i < playableCards.length; i++)
		{
			if(playableCards[i] == true)
			{
				playableCardExists = true;
				break;
			}
			else
			{
				playableCardExists = false;
			}
		}
	}
	
	public Deck getDeck()
	{
		return this.deck;
	}

	public boolean plusCardExisits(Card discardPileCard)
	{
		//returns true if a plus card exists in the deck (+2 or +4). Used for stacking
		setplayableCardInfo(discardPileCard); //refresh valid card bool[]
		for(int i = 0; i < this.deck.length(); i++)
		{
			if((this.deck.get(i).getValue() == Value.PLUS2 || this.deck.get(i).getValue() == Value.PLUS4) && this.playableCards[i])
			{
				return true;
			}
		}
		return false;
	}

	public void resetDeck()
	{
		//resets deck, for game restart
		deck = new Deck();
	}
}
