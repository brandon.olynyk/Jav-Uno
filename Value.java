public enum Value 
{
	//enum responsible for the values of cards. 1-9, and special cards
	ZERO,
	ONE,
	TWO,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	SKIP,
	REVERSE,
	PLUS2,
	PLUS4, //for some wildcards
	BLANK //for normal wildcards
}
